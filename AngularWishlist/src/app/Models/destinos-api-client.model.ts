
import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject} from 'rxjs';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
		DestinosViajesState,
		NuevoDestinoAction,
		ElegidoFavoritoAction
	} from './destinos-viajes-state.model';
//import {AppState} from './../app.module';
import {AppState, APP_CONFIG, AppConfig} from './../app.module';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
	//destinos:DestinoViaje[];
	//current: Subject<DestinoViaje>= new BehaviorSubject<DestinoViaje>(null);
		destinos:DestinoViaje[]=[];

	//constructor(private store: Store<AppState>) {
	constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
       //this.destinos = [];

			 this.store
	 .select(state => state.destinos)
	 .subscribe((data) => {
		 console.log("destinos sub store");
		 console.log(data);
		 this.destinos = data.items;
	 });

	 this.store
		 .subscribe((data) => {
			 console.log("all store");
			 console.log(data);
		 });

	}

	add(d:DestinoViaje){
	  //this.destinos.push(d);
		//this.store.dispatch(new NuevoDestinoAction(d));

		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
			}
		});
	}

	getAll(){
	  return this.destinos;
    }

	getById(id:String):DestinoViaje{
	  return this.destinos.filter(function(d){return d.nombre.toString() == id;})[0];
	}

	elegir(d: DestinoViaje) {
		/* this.destinos.forEach(x => x.setSelected(false));
		 d.setSelected(true);
		 this.current.next(d);*/
		 this.store.dispatch(new ElegidoFavoritoAction(d));
	 }

	 subscribeOnChange(fn){
		 // this.current.subscribe(fn);
	 }

}
