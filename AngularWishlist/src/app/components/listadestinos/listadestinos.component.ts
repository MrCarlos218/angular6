import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje} from './../../Models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css'],
  providers: [ DestinosApiClient]
})
export class ListadestinosComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>= new EventEmitter<DestinoViaje>();
  updates: string[];
  all;

  Destinos: DestinoViaje[];
  /*nombre:string;*/

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    //this.destinosApiClient.add(d);
    this.onItemAdded = new EventEmitter();
    this.updates = [];


    console.log('hola1');

      this.store.select(state => state.destinos.favorito)
          .subscribe(d => {
            //const fav = data;
         if(d != null){
              this.updates.push('Se ha elegido a: '+d.nombre.toString());
            }
          });

    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
    /*this.destinosApiClient.subscribeOnChange((d:DestinoViaje)=>{
      if(d != null){
        this.updates.push('Se ha elegido a: '+d.nombre.toString());
      }
    })*/
    console.log('hola2');

    this.Destinos=[];
  /*this.nombre="Whishlist";
  this.Destinos=[];*/
  /* 'Barcelona','Buenos Aires','Lima','Medellin' */
}

  ngOnInit(): void {

    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push("Se eligió: " + d.nombre);
        }
      });

  }


  agregado(d: DestinoViaje) //:boolean
  {
    //this.destinosApiClient.add(d);
    //this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));

    this.Destinos.push(d);
    /*this.Destinos.push(new DestinoViaje(nombre,url));
    console.log(this.Destinos);
    return false;*/
  }

  elegido(e:DestinoViaje){

      this.destinosApiClient.elegir(e);
      this.store.dispatch(new ElegidoFavoritoAction(e));

    /*this.Destinos.forEach(function (x) {x.setSelected(false);
      d.setSelected(true);*/
      /*this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
      e.setSelected(true);*/
    }

    getAll(){

    }

}
