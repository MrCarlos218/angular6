import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../Models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosViajesState, VoteUpAction, VoteDownAction } from './../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-destino',
  templateUrl: './destino.component.html',
  styleUrls: ['./destino.component.css']
})

export class DestinoComponent implements OnInit {
 @Input() Destino: DestinoViaje;
 @Input('idx') position: number;
 @HostBinding('attr.class') cssClass = 'col-md-4';
 @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.Destino);
    this.Destino.setSelected(true);
    return false;
  }

  voteup(){
    this.store.dispatch( new VoteUpAction(this.Destino));
    return false;
  }

  votedown(){
    this.store.dispatch( new VoteDownAction(this.Destino));
    return false;
  }

}
